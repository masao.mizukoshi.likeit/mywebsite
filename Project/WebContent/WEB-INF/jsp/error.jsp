<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>パソコンショップ</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="css/original/common.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
    .carousel-inner > .item > img,
    .carousel-inner > .item > a > img {
        width: 60%;
        margin: auto;
    }
  </style>
</head>
<body>
  <nav class="navbar navbar-inverse ">
    <div class="container-fluid">
      <div class="navbar-header"> <img class="logo" src="image/title.png"> </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="AdminUserList"><span class="glyphicon glyphicon-home"></span> ホーム</a></li>
        <li><a href="AdminUserList"><span class="glyphicon glyphicon-user"></span> ユーザ一覧</a></li>
        <li><a href="adminItemList.html"><span class="glyphicon glyphicon-shopping-cart"></span> 商品一覧</a></li>
        <li><a href="UserLogout"><span class="glyphicon glyphicon-log-out"></span> ログアウト</a></li>
      </ul>
    </div>
  </nav>
  <div class="container-fluid text-center">
    <div class="row content">
      <div class="col-sm-2 sidenav"> </div>
      <div class="col-sm-8 text-left">
        <h1>パソコンECサイト</h1>
        <p> </p>
        <hr>
        <div class="container">
          <div class="card form-container" style="font-size:16px;">
            <div class="row">
              <div class="col-sm-10">
                <h2>ユーザ 一覧</h2>
              </div>




              </div>
              <div class="row">
              <div>
              <c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" style="font-size:12px" role="alert">
		 			 ${errMsg}
				</div>
				</c:if>
               <button class="btn btn-lg btn-primary btn-block btn-signin" onClick="location.href='javascript:history.back()'">戻る</button>
              </div>
              </div>

            </div><br>
          </div>
          <!-- /card-container -->
        </div>
      </div>
      <div class="col-sm-2 sidenav"> </div>
    </div>

  <footer class="container-fluid text-center">
    <p>パソコンECサイト</p>
  </footer>
</body>
</html>