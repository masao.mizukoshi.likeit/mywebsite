<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>パソコンショップ</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="css/original/common.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
    .carousel-inner > .item > img,
    .carousel-inner > .item > a > img {
        width: 60%;
        margin: auto;
    }
  </style>
</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header"> <img class="logo" src="image/title.png"> </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="UserAdd"><span class="glyphicon glyphicon-user"></span> 新規登録</a></li>
        <li><a href="UserLogin"><span class="glyphicon glyphicon-log-in"></span> ログイン</a></li>
      </ul>
    </div>
  </nav>
  <div class="container-fluid text-center">
    <div class="row content">
      <div class="col-sm-2 sidenav"> </div>
      <div class="col-sm-8 text-left">
        <h1>パソコンECサイト</h1>
        <p></p>
        <hr>
        <h3>パソコン関連の商品あります。</h3>
        <p>デスクトップ、ノートパソコン、PCパーツ・周辺機器、PCソフトなどあります。</p>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active"> <img src="image/nexmag_style-m-810x450.jpg">
              <div class="carousel-caption"> </div>
            </div>
            <div class="item"> <img src="image/nexmag_style_nb50tz-810x450.png">
              <div class="carousel-caption"> </div>
            </div>
            <div class="item"> <img src="image/pcparts.png">
              <div class="carousel-caption"> </div>
            </div>
            <div class="item"> <img src="image/pcsoft.png">
              <div class="carousel-caption"> </div>
            </div>
          </div>
          <!-- Left and right controls --><a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a> <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a> </div>
      </div>
      <div class="col-sm-2 sidenav"> </div>
    </div>
  </div>
  <footer class="container-fluid text-center">
    <p>パソコンECサイト</p>
  </footer>
</body>
</html>