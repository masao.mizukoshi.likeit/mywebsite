<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>パソコンショップ</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="css/original/common.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header"> <img class="logo" src="image/title.png"> </div>
      <ul class="nav navbar-nav navbar-right">
    	<li><a href="UserAdd"><span class="glyphicon glyphicon-user"></span> 新規登録</a></li>
        <li><a href="UserLogin"><span class="glyphicon glyphicon-log-in"></span> ログイン</a></li>
      </ul>
    </div>
  </nav>

  <div class="container">
    <div class="card card-container">
      <p id="profile-name" class="profile-name-card">新規登録</p>
      <form class="form-signin" action="UserAdd" method="post">
      <span id="reauth-email" class="reauth-email"></span>
      ユーザー名<input type="text" name="name" class="form-control " placeholder="" required autofocus>
      ログインID<input type="text" name="loginId" class="form-control " placeholder="" required>
      パスワード<input type="password" name="password" class="form-control " placeholder="" required>
      パスワード確認<input type="password" name="passwordCheck" class="form-control " placeholder="" required>
      生年月日<input type="date" name="birthDate" max="2020-12-31" class="form-control"> <br>
      <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">登録</button>
      </form>
      <!-- /form --><a href="javascript:history.back()">戻る</a> </div>
    <!-- /card-container -->
  </div>


</body>
</html>