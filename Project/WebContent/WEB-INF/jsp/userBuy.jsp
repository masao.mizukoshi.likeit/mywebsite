<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>パソコンショップ</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="css/original/common.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
  <nav class="navbar navbar-inverse ">
    <div class="container-fluid">
      <div class="navbar-header"> <img class="logo" src="image/title.png"> </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="UserHome"><span class="glyphicon glyphicon-home"></span> ホーム</a></li>
        <li><a href="UserProfile"><span class="glyphicon glyphicon-user"></span> ${userInfo.name} さん</a></li>
        <li><a href="UserWish?user_id=${userInfo.id}"><span class="glyphicon glyphicon glyphicon-star-empty"></span> 欲しいもの</a></li>
        <li><a href="UserCart?user_id=${userInfo.id}"><span class="glyphicon glyphicon-shopping-cart"></span> カート</a></li>
        <li><a href="UserLogout"><span class="glyphicon glyphicon-log-out"></span> ログアウト</a></li>
      </ul>
      <form class="navbar-form navbar-right" action="ItemList">
        <div class="form-group">
        <select class="form-control" name="categoryId">
                <option value="">全てのカテゴリ</option>
                <option value="1" >デスクトップパソコン</option>
                <option value="2" >ノートパソコン</option>
                <option value="3" >PCパーツ・周辺機器</option>
                <option value="4" >PCソフト</option>
                </select>
                </div>
        <div class="form-group"> <input type="text" class="form-control" name="search_word" placeholder="キーワードで検索"> </div> <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
        </form>
    </div>
  </nav>
  <div class="container">
<div class="card card-box">
    <p id="profile-name" class="profile-name-card">購入完了しました！</p>
<a href="UserHome" class="btn btn-lg btn-primary btn-block btn-signin" type="button">ホーム画面へ</a>
</div>

  </div>
  <!-- /container -->
</body>
</html>