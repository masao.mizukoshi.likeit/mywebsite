<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>パソコンショップ</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="css/original/common.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
    .carousel-inner > .item > img,
    .carousel-inner > .item > a > img {
        width: 60%;
        margin: auto;
    }
  </style>
</head>
<body>
 <nav class="navbar navbar-inverse ">
    <div class="container-fluid">
      <div class="navbar-header"> <img class="logo" src="image/title.png"> </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="UserHome"><span class="glyphicon glyphicon-home"></span> ホーム</a></li>
        <li><a href="UserProfile"><span class="glyphicon glyphicon-user"></span> ${userInfo.name}さん</a></li>
        <li><a href="UserWish"><span class="glyphicon glyphicon glyphicon-star-empty"></span> 欲しいもの</a></li>
        <li><a href="UserCart"><span class="glyphicon glyphicon-shopping-cart"></span> カート</a></li>
        <li><a href="UserLogout"><span class="glyphicon glyphicon-log-out"></span> ログアウト</a></li>
      </ul>
      <form class="navbar-form navbar-right" action="ItemList">
        <div class="form-group">
        <select class="form-control" name="categoryId">
                <option value="">全てのカテゴリ</option>
                <option value="1" >デスクトップパソコン</option>
                <option value="2" >ノートパソコン</option>
                <option value="3" >PCパーツ・周辺機器</option>
                <option value="4" >PCソフト</option>
                </select>
                </div>
        <div class="form-group"> <input type="text" class="form-control" name="search_word" placeholder="キーワードで検索"> </div> <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
        </form>
    </div>
  </nav>
  <div class="container-fluid text-center">
    <div class="row content">
      <div class="col-sm-2 sidenav">
        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading"><strong>カテゴリ</strong></div>
            <div class="list-group">
            <a href="ItemList?search_word= &categoryId=1" class="list-group-item">デスクトップパソコン</a>
            <a href="ItemList?search_word= &categoryId=2" class="list-group-item">ノートパソコン</a>
            <a href="ItemList?search_word= &categoryId=3" class="list-group-item">PCパーツ・周辺機器</a>
            <a href="ItemList?search_word= &categoryId=4" class="list-group-item">PCソフト</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-8 text-left">
        <h1>パソコンECサイト</h1>
        <p> </p>
        <hr>
        <h3>パソコン関連の商品あります。</h3>
        <p>デスクトップ、ノートパソコン、PCパーツ・周辺機器、PCソフトなどあります。</p>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active"> <img src="image/nexmag_style-m-810x450.jpg">
              <div class="carousel-caption"> </div>
            </div>
            <div class="item"> <img src="image/nexmag_style_nb50tz-810x450.png">
              <div class="carousel-caption"> </div>
            </div>
            <div class="item"> <img src="image/pcparts.png">
              <div class="carousel-caption"> </div>
            </div>
            <div class="item"> <img src="image/pcsoft.png">
              <div class="carousel-caption"> </div>
            </div>
          </div>
          <!-- Left and right controls --><a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a> <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a> </div>
      </div>
      <div class="col-sm-2 sidenav"> </div>

    </div>
  </div>
  <footer class="container-fluid text-center">
    <p>パソコンECサイト</p>
  </footer>
</body>
</html>