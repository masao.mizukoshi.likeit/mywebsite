<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>パソコンショップ</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="css/original/common.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header"> <img class="logo" src="image/title.png"> </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="UserAdd"><span class="glyphicon glyphicon-user"></span> 新規登録</a></li>
        <li><a href="UserLogin"><span class="glyphicon glyphicon-log-in"></span> ログイン</a></li>
      </ul>
    </div>
  </nav>
  <div class="container">
    <div class="card card-container"> <img id="profile-img" class="profile-img-card" src="./image/avatar_2x.png" />
      <p id="profile-name" class="profile-name-card"></p>
      <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
      <form class="form-signin" action="UserLogin" method="post">
      <span id="reauth-email" class="reauth-email"></span>
      <input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID" required autofocus>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード" required>
      <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">ログイン</button>
      </form>
      <!-- /form --><a href="Index">戻る</a> </div>
    <!-- /card-container -->
  </div>
  <!-- /container -->
</body>
</html>