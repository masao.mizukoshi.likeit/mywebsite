<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ユーザ一覧</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="css/original/common.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
  <nav class="navbar navbar-inverse ">
    <div class="container-fluid">
      <div class="navbar-header"> <img class="logo" src="image/title.png"> </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="UserHome"><span class="glyphicon glyphicon-home"></span> ホーム</a></li>
        <li><a href="UserProfile"><span class="glyphicon glyphicon-user"></span> ${userInfo.name}さん</a></li>
        <li><a href="UserWish"><span class="glyphicon glyphicon glyphicon-star-empty"></span> 欲しいもの</a></li>
        <li><a href="UserCart"><span class="glyphicon glyphicon-shopping-cart"></span> カート</a></li>
        <li><a href="UserLogout"><span class="glyphicon glyphicon-log-out"></span> ログアウト</a></li>
      </ul>
      <form class="navbar-form navbar-right" action="ItemList">
        <div class="form-group">
        <select class="form-control" name="categoryId">
                <option value="">全てのカテゴリ</option>
                <option value="1" >デスクトップパソコン</option>
                <option value="2" >ノートパソコン</option>
                <option value="3" >PCパーツ・周辺機器</option>
                <option value="4" >PCソフト</option>
                </select>
                </div>
        <div class="form-group"> <input type="text" class="form-control" name="search_word" placeholder="キーワードで検索"> </div> <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
        </form>
    </div>
  </nav>
  <div class="container-fluid text-center">
    <div class="row content">
     <div class="col-sm-2 sidenav">
        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading"><strong>カテゴリ</strong></div>
            <div class="list-group">
            <a href="ItemList?search_word= &categoryId=1" class="list-group-item">デスクトップパソコン</a>
            <a href="ItemList?search_word= &categoryId=2" class="list-group-item">ノートパソコン</a>
            <a href="ItemList?search_word= &categoryId=3" class="list-group-item">PCパーツ・周辺機器</a>
            <a href="ItemList?search_word= &categoryId=4" class="list-group-item">PCソフト</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-8 text-left">
        <h3></h3>
        <hr>
        <div class="container">
          <div class="card form-container" style="font-size:16px;">
            <h2>ユーザ情報</h2>
            <div class="row">
             <c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" style="font-size:12px" role="alert">
		 			 ${errMsg}
				</div>
				</c:if>
              <div class="col-sm-6"> <label>ログインID</label>
                <p>${userInfo.loginId}</p>
              </div>
              <div class="col-sm-6"> <label>ユーザ名</label>
                <p>${userInfo.name}</p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6"> <label>生年月日</label>
                <p>${userInfo.birthdate}</p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12"> <button class="btn btn-lg btn-primary btn-block btn-signin" data-toggle="modal" data-target="#editInfo">編集</button>
                <div class="modal fade" id="editInfo" role="dialog">
                  <div class="modal-dialog">
                    <!----商品詳細の表示---->
                    <div class="modal-content">
                      <div class="modal-header itempanel"> <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">ユーザ情報</h4>
                      </div>
                      <div class="modal-body">
                        <form class="form-signin" action="UserProfile" method="post"> <span id="reauth-email" class="reauth-email"></span>

                          <p>ログインID userInfo.loginId</p>
                          <input type=hidden class="form-control "name="loginId" value="${userInfo.loginId}">
                          ユーザー名<input type="text" name="name" class="form-control " value="${userInfo.name}" autofocus required>
                          生年月日<input type="date" name="birthDate" max="2020-12-31" class="form-control" value="${userInfo.birthdate}" required>
                          パスワード<input type="password" name="password" class="form-control " placeholder="">
                          パスワード確認<input type="password" name="passwordCheck" class="form-control " placeholder="">
                           <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">更新</button>
                         </form>

                      </div>
                      <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /card-container -->
        </div>
        <div class="container">
          <div class="card form-container" style="font-size:16px;">
            <h2>購入履歴</h2>
            <table class="table">
              <thead>
                <tr>
                  <th>購入日時</th>
                  <th>購入金額</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              <c:forEach var="buy" items="${buyHistory}">
                <tr>
                  <td>${buy.formatDate}</td>
                  <td>${buy.totalPrice}円(税込み)</td>
                  <td><button type="button" class="btn btn-lg btn-primary btn-block btn-signin" data-toggle="modal" data-target="#buydetail${buy.id}">詳細</button>

              </td>
              </tr>
              </c:forEach>
              </tbody>
            </table>
<c:forEach var="buyDetail1" items="${buyDetailHistory}">
            <div class="modal fade" id="buydetail${buyDetail1.buyDataBeans.id}" role="dialog">
                      <div class="modal-dialog modal-lg">
                        <!----商品詳細の表示---->
                        <div class="modal-content">
                          <div class="modal-header itempanel"> <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">購入履歴詳細 ${buyDetail1.buyDataBeans.formatDate}</h4>
                          </div>
                          <div class="modal-body">
                            <table class="table">
                              <thead>
                                <tr>
                                  <th>商品名</th>
                                  <th>数量</th>
                                  <th class="text-right">価格</th>
                                </tr>
                              </thead>
                              <tbody>
<c:forEach var="buyItem" items="${buyDetailHistory}">
<c:choose>
<c:when test="${buyDetail1.buyDataBeans.id == buyItem.buyDataBeans.id}">
<tr>
                                  <td><a data-toggle="modal" data-target="#item${buyItem.itemDataBeans.id}" style="text-decoration:none;cursor: pointer">${buyItem.itemDataBeans.name} </a></td>
                                  <td>1</td>
                                  <td class="text-right">${buyItem.itemDataBeans.price}円</td>
                                </tr></c:when>
</c:choose>

                                </c:forEach>
                                <tr>
                                  <td>消費税(8%)</td>
                                  <td></td>
                                  <td class="text-right">${buyDetail1.buyDataBeans.taxPrice}円</td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td class="text-right"><strong>合計金額</strong> ${buyDetail1.buyDataBeans.totalPrice}円</td>
                                </tr>
                              </tbody>
              </table>
              </div>
              <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
              </div>
              </div>
              </div>
              </div>
              </c:forEach>






<c:forEach var="item" items="${buyDetailHistory}">
            <div class="modal fade" id="item${item.itemDataBeans.id}" role="dialog">
              <div class="modal-dialog">
                <!----商品詳細の表示---->
                <div class="modal-content">
                  <div class="modal-header itempanel"> <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">商品詳細</h4>
                  </div>
                  <div class="modal-body">
                    <p style="font-size:16px"><strong>${item.itemDataBeans.name}</strong></p>
                    <p style="font-size:16px"><strong>${item.itemDataBeans.price}円(税抜)</strong></p> <img src="image/${item.itemDataBeans.fileName}" class="img-responsive center-block" style="width:55%" alt="Image">
                    <p style="font-size:12px"><strong>${item.itemDataBeans.detail}</strong></p>
                  </div>
                  <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button> </div>

                </div>
              </div>
            </div>
            </c:forEach>
          </div>
          <!-- /card-container -->
        </div>
        <div class="col-sm-2 sidenav"> </div>
      </div>
    </div>
  </div>
  <footer class="container-fluid text-center">
    <p>パソコンECサイト</p>
  </footer>
</body>
</html>