<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>欲しいものリスト</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="css/original/common.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
 <nav class="navbar navbar-inverse ">
    <div class="container-fluid">
      <div class="navbar-header"> <img class="logo" src="image/title.png"> </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="UserHome"><span class="glyphicon glyphicon-home"></span> ホーム</a></li>
        <li><a href="UserProfile"><span class="glyphicon glyphicon-user"></span> ${userInfo.name}さん</a></li>
        <li><a href="UserWish"><span class="glyphicon glyphicon glyphicon-star-empty"></span> 欲しいもの</a></li>
        <li><a href="UserCart"><span class="glyphicon glyphicon-shopping-cart"></span> カート</a></li>
        <li><a href="UserLogout"><span class="glyphicon glyphicon-log-out"></span> ログアウト</a></li>
      </ul>
      <form class="navbar-form navbar-right" action="ItemList">
        <div class="form-group">
        <select class="form-control" name="categoryId">
                <option value="">全てのカテゴリ</option>
                <option value="1" >デスクトップパソコン</option>
                <option value="2" >ノートパソコン</option>
                <option value="3" >PCパーツ・周辺機器</option>
                <option value="4" >PCソフト</option>
                </select>
                </div>
        <div class="form-group"> <input type="text" class="form-control" name="search_word" placeholder="キーワードで検索"> </div> <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
        </form>
    </div>
  </nav>
  <div class="container-fluid text-center">
    <div class="row content">
      <div class="col-sm-2 sidenav">
        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading"><strong>カテゴリ</strong></div>
            <div class="list-group">
            <a href="ItemList?search_word= &categoryId=1" class="list-group-item">デスクトップパソコン</a>
            <a href="ItemList?search_word= &categoryId=2" class="list-group-item">ノートパソコン</a>
            <a href="ItemList?search_word= &categoryId=3" class="list-group-item">PCパーツ・周辺機器</a>
            <a href="ItemList?search_word= &categoryId=4" class="list-group-item">PCソフト</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-8 text-left">
        <h1>欲しいものリスト</h1>
         <h3>${wishItemCount}点 あります</h3>
        <hr>
        <p></p>
        <div class="container">
          <div class="row">
            <c:forEach var="wish" items="${wishList}">

            <div class="col-sm-4">
              <div class="panel panel-default">
                <div class="panel-heading itempanel" data-toggle="modal" data-target="#item${wish.itemDataBeans.id}">
                <strong>${wish.itemDataBeans.name}</strong>
                </div>
                <div class="panel-body" data-toggle="modal" data-target="#item${wish.itemDataBeans.id}">
                <img src="image/${wish.itemDataBeans.fileName}" class="img-responsive center-block" style="width:55%" >
                </div>
                <div class="panel-footer">
                	<button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#item${wish.itemDataBeans.id}">詳細</button>
                	<p class="price" style="font-size:16px" data-toggle="modal" data-target="#item${wish.itemDataBeans.id}">
                	<strong>${wish.itemDataBeans.price}円(税抜)</strong></p>
                </div>
              </div>
            </div>
            	<!----商品詳細の表示---->
                  <div class="modal fade" id="item${wish.itemDataBeans.id}" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header itempanel"> <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">商品詳細</h4>
                        </div>
                        <div class="modal-body">
                          <p style="font-size:16px"><strong>${wish.itemDataBeans.name}</strong></p>
                          <p style="font-size:16px"><strong>${wish.itemDataBeans.price}円(税抜)</strong></p>
                          <img src="image/${wish.itemDataBeans.fileName}" class="img-responsive center-block" style="width:55%" alt="Image">
                          <p style="font-size:12px"><strong>${wish.itemDataBeans.detail}</strong></p>
                        </div>
                        <div class="modal-footer">
						<form action="UserCart" method="post">
                        <input type="hidden" name="item_id" class="form-control"value="${wish.itemDataBeans.id}">
                        <input type="hidden" name="user_id" class="form-control"value="${userInfo.id}">
                        <button type="submit" class="btn btn-default pull-left" >カートに追加</button>
                        </form>
                        <a href="DeleteWishItem?wish_id=${wish.id}" type="button" class="btn btn-default pull-left" >削除</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
                        </div>

                      </div>
                    </div>
                  </div>
</c:forEach>
          </div>
        </div>
        <div class="text-center">
          <ul class="pagination">

          <!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<a class="disabled">&laquo;</a>
					</c:when>
					<c:otherwise>
						<a href="UserWish?user_id=${userId}&page_num=${pageNum - 1}">&laquo;</a>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li <c:if test="${pageNum == status.index }" ></c:if>><a href="UserWish?user_id=${userId}&page_num=${status.index}" class="active"> ${status.index} </a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<a class="disabled">&raquo;</a>
				</c:when>
				<c:otherwise>
					<a href="UserWish?user_id=${userId}&page_num=${pageNum + 1}">&raquo;</a>
				</c:otherwise>
				</c:choose>
         </ul>
        </div>
      </div>
      <div class="col-sm-2 sidenav"> </div>
    </div>
  </div>
  <footer class="container-fluid text-center">
    <p>パソコンECサイト</p>
  </footer>
</body>
</html>