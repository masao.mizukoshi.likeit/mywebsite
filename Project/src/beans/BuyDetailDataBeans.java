package beans;

import java.io.Serializable;
import java.sql.SQLException;

import dao.BuyDAO;
import dao.ItemDAO;

public class BuyDetailDataBeans implements Serializable {

	private int id;
	private int buyId;
	private int itemId;
	private int cartQuantity;



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBuyId() {
		return buyId;
	}
	public void setBuyId(int buyId) {
		this.buyId = buyId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getCartQuantity() {
		return cartQuantity;
	}
	public void setCartQuantity(int cartQuantity) {
		this.cartQuantity = cartQuantity;
	}

	public ItemDataBeans getItemDataBeans() {
		try {
			return ItemDAO.getItemByItemID(String.valueOf(this.itemId));
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}


	public BuyDataBeans getBuyDataBeans() {
		try {
			return BuyDAO.getBuyDataByBuyId(this.buyId);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return null;
		}
	}






}
