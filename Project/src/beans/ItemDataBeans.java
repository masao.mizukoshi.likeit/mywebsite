package beans;

import java.io.Serializable;

public class ItemDataBeans implements Serializable{

	private int id;
	private int categoryId;
	private String name;
	private int price;
	private int stockQuantity;
	private String fileName;
	private String detail;




	public ItemDataBeans(int id, int categoryId, String name, int price, int stockQuantity, String fileName,
			String detail) {
		this.id = id;
		this.categoryId = categoryId;
		this.name = name;
		this.price = price;
		this.stockQuantity = stockQuantity;
		this.fileName = fileName;
		this.detail = detail;
	}
	public ItemDataBeans() {

	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}













}
