package beans;

import java.io.Serializable;
import java.sql.SQLException;

import dao.ItemDAO;

public class WishDataBeans implements Serializable {

	private int id;
	private int userId;
	private int itemId;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public ItemDataBeans getItemDataBeans() {
		try {
			return ItemDAO.getItemByItemID(String.valueOf(this.itemId));
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
