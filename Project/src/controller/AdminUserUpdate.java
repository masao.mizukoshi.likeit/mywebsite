package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDAO;

/**
 * Servlet implementation class AdminUserUpdate
 */
@WebServlet("/AdminUserUpdate")
public class AdminUserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminUserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		int userId= Integer.parseInt(request.getParameter("user_id"));
		String name= request.getParameter("name");
		String birthDate= request.getParameter("birth_date");
		String password= request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");

		UserDAO userDao = new UserDAO();
		if(!(password.equals(passwordCheck))) {
			request.setAttribute("errMsg", "パスワードが異なります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);

			return;
		}

		if(password.equals("")){

			userDao.UpdateUser(userId,name,birthDate);
			response.sendRedirect("AdminUserList");
			return;
		}

			userDao.UpdateUserWithPassword(userId,name,birthDate,password);
			response.sendRedirect("AdminUserList");
	}

}
