package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.CartDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;
import dao.UserCartDAO;

/**
 * Servlet implementation class BuyResult
 */
@WebServlet("/BuyResult")
public class BuyResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyResult() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
		ArrayList<CartDataBeans> cartList = (ArrayList<CartDataBeans>)session.getAttribute("cartList");
		session.removeAttribute("cartList");
		BuyDataBeans bdb = (BuyDataBeans) session.getAttribute("bdb");

		UserCartDAO userCartDao = new UserCartDAO();
			int buyId = BuyDAO.insertBuy(bdb);

			for(CartDataBeans cartInItem : cartList) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setBuyId(buyId);
				bddb.setItemId(cartInItem.getItemId());
				BuyDetailDAO.insertBuyDetail(bddb);
				userCartDao.DeleteCartItem(String.valueOf(cartInItem.getId()));
			}


		} catch (SQLException e) {

			e.printStackTrace();
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userBuy.jsp");
		dispatcher.forward(request, response);





	}

}
