package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemList
 */
@WebServlet("/ItemList")
public class ItemList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemList() {
        super();
        // TODO Auto-generated constructor stub
    }
    final static int PAGE_MAX_ITEM_COUNT =6;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserDataBeans status= (UserDataBeans) session.getAttribute("userInfo");
		ItemDAO itemDao = new ItemDAO();
		if(status ==null) {
			response.sendRedirect("Index");
			return;
			}



			String searchWord = request.getParameter("search_word");
			String categoryId = request.getParameter("categoryId");




		int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
		// 新たに検索されたキーワードをセッションに格納する
		session.setAttribute("searchWord", searchWord);
		session.setAttribute("categoryId", categoryId);

		// 商品リストを取得 ページ表示分のみ
			List<ItemDataBeans> searchResultItemList;
			try {
				searchResultItemList = itemDao.getItemsByKeyword(searchWord, categoryId,pageNum, PAGE_MAX_ITEM_COUNT);

				// 検索ワードに対しての総ページ数を取得
				double itemCount = ItemDAO.getItemCount(searchWord,categoryId);
				int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

				//総アイテム数
				request.setAttribute("itemCount", (int) itemCount);
				// 総ページ数
				request.setAttribute("pageMax", pageMax);
				// 表示ページ
				request.setAttribute("pageNum", pageNum);
				request.setAttribute("itemList", searchResultItemList);
			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemList.jsp");
			dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
