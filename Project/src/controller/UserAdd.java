package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/UserAdd")
public class UserAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");



		// リクエストパラメータの入力項目を取得

		String name= request.getParameter("name");
		String loginId= request.getParameter("loginId");
		String password= request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		String birthDate= request.getParameter("birthDate");



		if(!(password.equals(passwordCheck))) {
			request.setAttribute("errMsg", "パスワードが異なります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);
			return;
		}

		HttpSession session = request.getSession();

		UserDataBeans status= (UserDataBeans) session.getAttribute("userInfo");
		UserDAO userDao = new UserDAO();
		userDao.InsertUser(name,loginId,password,birthDate);

		if(status ==null) {

			response.sendRedirect("Index");
			return;
	}


		response.sendRedirect("AdminUserList");
	}

}
