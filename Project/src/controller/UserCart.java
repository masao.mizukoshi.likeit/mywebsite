package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.CartDataBeans;
import beans.UserDataBeans;
import dao.UserCartDAO;

/**
 * Servlet implementation class Index
 */
@WebServlet("/UserCart")
public class UserCart extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCart() {
        super();
        // TODO Auto-generated constructor stub
    }
    final static int PAGE_MAX_ITEM_COUNT =6;
    final static double TAX =0.08;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserDataBeans status= (UserDataBeans) session.getAttribute("userInfo");

		if(status ==null) {
			response.sendRedirect("Index");
			return;

			}
		int userId= (int) session.getAttribute("userId");

		int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
		UserCartDAO userCartDao = new UserCartDAO();
		List<CartDataBeans> resultCartList;

		try {
			resultCartList = userCartDao.getCartItemsByUserId(userId,pageNum, PAGE_MAX_ITEM_COUNT);
			// 検索ワードに対しての総ページ数を取得
			double cartItemCount = UserCartDAO.getCartItemCount(userId);
			int pageMax = (int) Math.ceil(cartItemCount / PAGE_MAX_ITEM_COUNT);


			//総アイテム数
			request.setAttribute("itemCount", (int) cartItemCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			session.setAttribute("cartList", resultCartList);
			ArrayList<CartDataBeans> cartList = (ArrayList<CartDataBeans>)session.getAttribute("cartList");
			int totalPrice = getTotalItemPrice(cartList);
			int tax = (int) Math.ceil(totalPrice * TAX);

			BuyDataBeans bdb = new BuyDataBeans();
			bdb.setUserId(userId);
			bdb.setTotalPrice(totalPrice + tax);
			bdb.setTaxPrice(tax);

			session.setAttribute("bdb", bdb);
			request.setAttribute("tax", tax);
			request.setAttribute("totalPrice",totalPrice);

			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCart.jsp");
			dispatcher.forward(request, response);


		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		String itemId = request.getParameter("item_id");
		int userId= (int) session.getAttribute("userId");

		UserCartDAO userCartDao = new UserCartDAO();
		userCartDao.AddCartItem(userId, itemId);

		response.sendRedirect("UserCart");
	}

	public static int getTotalItemPrice(ArrayList<CartDataBeans> items) {
		int total = 0;

		for(CartDataBeans item : items) {
			item.getItemDataBeans();
			total+=item.getItemDataBeans().getPrice();
		}

		return total;
	}




}
