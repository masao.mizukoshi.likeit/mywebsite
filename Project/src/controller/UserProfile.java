package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserProfile
 */
@WebServlet("/UserProfile")
public class UserProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		UserDataBeans status= (UserDataBeans) session.getAttribute("userInfo");


		if(status ==null) {
			response.sendRedirect("Index");
			return;
	}
		int userId= (int) session.getAttribute("userId");

		ArrayList<BuyDataBeans> buyHistory;
		ArrayList<BuyDetailDataBeans> buyDetailHistory;

		try {
			buyHistory = BuyDAO.getBuyDataBeansByUserId(userId);
			session.setAttribute("buyHistory", buyHistory);
			buyDetailHistory=BuyDetailDAO.getBuyDetailDataBeansByUserId(userId);
			session.setAttribute("buyDetailHistory", buyDetailHistory);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userProfile.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		// リクエストパラメータの入力項目を取得
		int userId= (int) session.getAttribute("userId");
		String loginId =request.getParameter("loginId");
		String name= request.getParameter("name");
		String birthDate= request.getParameter("birthDate");
		String password= request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");

		UserDAO userDao = new UserDAO();
		if(!(password.equals(passwordCheck))) {
			request.setAttribute("errMsg", "パスワードが異なります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userProfile.jsp");
			dispatcher.forward(request, response);

			return;
		}

		if(password.equals("")){

			userDao.UpdateUser(userId,name,birthDate);
			session.removeAttribute("userInfo");
			UserDataBeans user = userDao.findByLoginInfo(loginId);
			session.setAttribute("userInfo", user);
			response.sendRedirect("UserProfile");
			return;
		}
		else {
			userDao.UpdateUserWithPassword(userId,name,birthDate,password);
			session.removeAttribute("userInfo");
			UserDataBeans user = userDao.findByLoginInfo(loginId);
			session.setAttribute("userInfo", user);
			response.sendRedirect("UserProfile");
			return;
		}


	}

}
