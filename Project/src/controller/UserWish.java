package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import beans.WishDataBeans;
import dao.UserWishDAO;

/**
 * Servlet implementation class Index
 */
@WebServlet("/UserWish")
public class UserWish extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserWish() {
        super();
        // TODO Auto-generated constructor stub
    }
    final static int PAGE_MAX_ITEM_COUNT =6;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserDataBeans status= (UserDataBeans) session.getAttribute("userInfo");


		if(status ==null) {
			response.sendRedirect("Index");
			return;
			}



		int userId= (int) session.getAttribute("userId");
		int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
		UserWishDAO userWishDao = new UserWishDAO();
		List<WishDataBeans> resultWishList;
		try {

			resultWishList = userWishDao.getWishItemsByUserId(userId,pageNum, PAGE_MAX_ITEM_COUNT);
			// 検索ワードに対しての総ページ数を取得
			double wishItemCount = UserWishDAO.getWishItemCount(userId);
			int pageMax = (int) Math.ceil(wishItemCount / PAGE_MAX_ITEM_COUNT);
			//総アイテム数
			request.setAttribute("wishItemCount", (int) wishItemCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("wishList", resultWishList);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userWish.jsp");
				dispatcher.forward(request, response);


			}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		UserDataBeans status= (UserDataBeans) session.getAttribute("userInfo");


		if(status ==null) {
			response.sendRedirect("Index");
			return;
			}


		String itemId = request.getParameter("item_id");
		int userId= (int) session.getAttribute("userId");

		UserWishDAO userWishDao = new UserWishDAO();
		userWishDao.AddWishItem(userId,itemId);


		response.sendRedirect("UserWish");


	}

}