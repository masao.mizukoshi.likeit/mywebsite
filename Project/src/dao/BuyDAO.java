package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DBManager.DBManager;
import beans.BuyDataBeans;

public class BuyDAO {

	public static int insertBuy(BuyDataBeans bdb) throws SQLException {
		Connection con = null;
		PreparedStatement pStmt = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			pStmt = con.prepareStatement(
					"INSERT INTO t_buy(user_id,total_price,tax_price,create_date) VALUES(?,?,?,now())",
					Statement.RETURN_GENERATED_KEYS);
			pStmt.setInt(1, bdb.getUserId());
			pStmt.setInt(2, bdb.getTotalPrice());
			pStmt.setInt(3, bdb.getTaxPrice());

			pStmt.executeUpdate();

			ResultSet rs = pStmt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	public static ArrayList<BuyDataBeans> getBuyDataBeansByUserId(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement pStmt = null;



		try {
			con = DBManager.getConnection();
			ArrayList<BuyDataBeans> buyList = new ArrayList<BuyDataBeans>();
			pStmt = con.prepareStatement(
					"SELECT * FROM t_buy WHERE user_id = ? ORDER BY id DESC");
			pStmt.setInt(1, userId);

			ResultSet rs = pStmt.executeQuery();


			while (rs.next()) {
				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setId(rs.getInt("id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setTaxPrice(rs.getInt("tax_price"));
				bdb.setBuyDate(rs.getTimestamp("create_date"));
				buyList.add(bdb);

			}

			return buyList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}



	public static BuyDataBeans getBuyDataByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_buy WHERE id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();

			BuyDataBeans bdb = new BuyDataBeans();
			if (rs.next()) {
				bdb.setId(rs.getInt("id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setTaxPrice(rs.getInt("tax_price"));
				bdb.setBuyDate(rs.getTimestamp("create_date"));

			}



			return bdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


}
