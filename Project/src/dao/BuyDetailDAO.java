package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DBManager.DBManager;
import beans.BuyDetailDataBeans;

public class BuyDetailDAO {

	public static void insertBuyDetail(BuyDetailDataBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement pStmt = null;
		try {
			con = DBManager.getConnection();
			pStmt = con.prepareStatement(
					"INSERT INTO t_buy_detail(buy_id,item_id,cart_quantity) VALUES(?,?,1)");
			pStmt.setInt(1, bddb.getBuyId());
			pStmt.setInt(2, bddb.getItemId());
			pStmt.executeUpdate();


		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<BuyDetailDataBeans> getItemDataBeansByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;



		try {
			con = DBManager.getConnection();
			ArrayList<BuyDetailDataBeans> itemList = new ArrayList<BuyDetailDataBeans>();
			st = con.prepareStatement(
					"SELECT *"
					+ " FROM t_buy_detail t"
					+ " INNER JOIN m_item m"
					+ " ON t.item_id = m.id"
					+ " WHERE t.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();


			while (rs.next()) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setBuyId(rs.getInt("t.buy_id"));
				bddb.setItemId(rs.getInt("m.id"));
				itemList.add(bddb);
			}




			return itemList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static ArrayList<BuyDetailDataBeans> getBuyDetailDataBeansByUserId(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement pStmt = null;



		try {
			con = DBManager.getConnection();
			ArrayList<BuyDetailDataBeans> buyList = new ArrayList<BuyDetailDataBeans>();
			pStmt = con.prepareStatement(
					"SELECT * FROM t_buy t INNER JOIN t_buy_detail t2 ON t.id = t2.buy_id WHERE user_id = ?");
			pStmt.setInt(1, userId);

			ResultSet rs = pStmt.executeQuery();


			while (rs.next()) {
				BuyDetailDataBeans bdb = new BuyDetailDataBeans();
				bdb.setBuyId(rs.getInt("t2.buy_id"));
				bdb.setItemId(rs.getInt("t2.item_id"));

				buyList.add(bdb);

			}

			return buyList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}


}


