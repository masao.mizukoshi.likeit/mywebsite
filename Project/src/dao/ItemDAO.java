package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import DBManager.DBManager;
import beans.ItemDataBeans;

public class ItemDAO {


	public List<ItemDataBeans> findAllItem() {
		Connection conn = null;
		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();


			String sql = "SELECT * FROM m_item";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);


			while (rs.next()) {

				int id = rs.getInt("id");
				int categorytId = rs.getInt("category_id");
				String name = rs.getString("name");
				int price =rs.getInt("price");
				int stockQuantity =rs.getInt("stock_quantity");
				String fileName = rs.getString("file_name");
				String detail = rs.getString("detail");

				ItemDataBeans item = new ItemDataBeans(id, categorytId, name, price, stockQuantity, fileName, detail);

				itemList.add(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//商品検索
	public List<ItemDataBeans> getItemsByKeyword(String searchWord,String categoryId,int pageNum, int pageMaxItemCount) throws SQLException {
		Connection conn = null;
		PreparedStatement st = null;
		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;

			//カテゴリIDを紐づいて商品を表示する
			if (!categoryId.equals("")){

				//キーワード入力欄が空
				if (searchWord.length() == 0){
					// カテゴリIdに紐づいて全検索
					st = conn.prepareStatement("SELECT * FROM m_item WHERE category_id =? ORDER BY id ASC LIMIT ?,? ");
					st.setString(1, categoryId);
					st.setInt(2, startiItemNum);
					st.setInt(3, pageMaxItemCount);
				}
				else {
					st = conn.prepareStatement("SELECT * FROM m_item WHERE (detail LIKE ? OR name LIKE ?) AND category_id=? ORDER BY id ASC LIMIT ?,? ");
					st.setString(1,"%"+searchWord+"%");
					st.setString(2,"%"+searchWord+"%");
					st.setString(3,categoryId);
					st.setInt(4, startiItemNum);
					st.setInt(5, pageMaxItemCount);

				}
			}
			else {
				//キーワード入力欄が空
				if (searchWord.length() == 0){
					// 全検索
					st = conn.prepareStatement("SELECT * FROM m_item ORDER BY id ASC LIMIT ?,? ");
					st.setInt(1, startiItemNum);
					st.setInt(2, pageMaxItemCount);
				}
				else {
					st = conn.prepareStatement("SELECT * FROM m_item WHERE detail LIKE ? OR name LIKE ?  ORDER BY id ASC LIMIT ?,? ");
					st.setString(1,"%"+searchWord+"%");
					st.setString(2,"%"+searchWord+"%");
					st.setInt(3, startiItemNum);
					st.setInt(4, pageMaxItemCount);

				}
			}
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setCategoryId(rs.getInt("category_id"));
				item.setName(rs.getString("name"));
				item.setPrice(rs.getInt("price"));
				item.setStockQuantity(rs.getInt("stock_quantity"));
				item.setFileName(rs.getString("file_name"));
				item.setDetail(rs.getString("detail"));

				itemList.add(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//商品結果の検索をカウントする
	public static double getItemCount(String searchWord,String categoryId) throws SQLException {
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DBManager.getConnection();
			st = conn.prepareStatement("select count(*) as cnt from m_item where name like ? or detail like ?");
			if (!categoryId.equals("")){

				if (searchWord.length() == 0){

					st = conn.prepareStatement("SELECT count(*) as cnt FROM m_item WHERE category_id =?");
					st.setString(1, categoryId);
				}
				else {
					st = conn.prepareStatement("SELECT count(*) as cnt FROM m_item WHERE (detail LIKE ? OR name LIKE ?) AND category_id=?");
					st.setString(1,"%"+searchWord+"%");
					st.setString(2,"%"+searchWord+"%");
					st.setString(3,categoryId);

				}
			}
			else {
				if (searchWord.length() == 0){
					// 全検索
					st = conn.prepareStatement("SELECT count(*) as cnt FROM m_item");
				}
				else {
					st = conn.prepareStatement("SELECT count(*) as cnt FROM m_item WHERE detail LIKE ? OR name LIKE ?");
					st.setString(1,"%"+searchWord+"%");
					st.setString(2,"%"+searchWord+"%");
				}
			}
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	public static ItemDataBeans getItemByItemID(String itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item WHERE id = ?");
			st.setString(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
			}



			return item;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ItemDataBeans getItemByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item m INNER JOIN t_buy_detail t ON m.id = t.item_id WHERE t.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
			}



			return item;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


}
