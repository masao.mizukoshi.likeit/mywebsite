package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DBManager.DBManager;
import beans.CartDataBeans;

public class UserCartDAO {

	//ユーザの新規登録
			public void AddCartItem(int userId, String itemId) {
				Connection conn = null;
				PreparedStatement pStmt = null;
				try {
					// データベースへ接続
					conn = DBManager.getConnection();

					String sql = "INSERT INTO t_cart (user_id,item_id,quantity) VALUES(?,?,1)";

					pStmt = conn.prepareStatement(sql);
					// SQLの?パラメータに値を設定
					pStmt.setInt(1, userId);
					pStmt.setString(2, itemId);

					// 登録SQL実行
					pStmt.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {
						// ステートメントインスタンスがnullでない場合、クローズ処理を実行
						if (pStmt != null) {
							pStmt.close();
						}
						// コネクションインスタンスがnullでない場合、クローズ処理を実行
						if (conn != null) {
							conn.close();
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

			}
			public void DeleteCartItem(String cartId) {
				Connection conn = null;
				PreparedStatement pStmt = null;
				try {
					// データベースへ接続
					conn = DBManager.getConnection();

					String sql = "DELETE FROM t_cart WHERE id=?";

					pStmt = conn.prepareStatement(sql);
					// SQLの?パラメータに値を設定
					pStmt.setString(1, cartId);


					// 登録SQL実行
					pStmt.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {
						// ステートメントインスタンスがnullでない場合、クローズ処理を実行
						if (pStmt != null) {
							pStmt.close();
						}
						// コネクションインスタンスがnullでない場合、クローズ処理を実行
						if (conn != null) {
							conn.close();
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

			}

			//カート表示
			public List<CartDataBeans> getCartItemsByUserId(int userId,int pageNum, int pageMaxItemCount) throws SQLException {
				Connection conn = null;
				PreparedStatement st = null;
				List<CartDataBeans> CartList = new ArrayList<CartDataBeans>();

				try {
					// データベースへ接続
					conn = DBManager.getConnection();
					int startiItemNum = (pageNum - 1) * pageMaxItemCount;


					String sql="SELECT * FROM t_cart t "
							+ "INNER JOIN m_item m "
							+ "ON t.item_id = m.id "
							+ "WHERE t.user_id =? "
							+ "ORDER BY t.id "
							+ "ASC LIMIT ?,? ";
							st = conn.prepareStatement(sql);
							st.setInt(1,userId);
							st.setInt(2, startiItemNum);
							st.setInt(3, pageMaxItemCount);

					ResultSet rs = st.executeQuery();
					while (rs.next()) {

						CartDataBeans beans = new CartDataBeans();
						beans.setId(rs.getInt("t.id"));
						beans.setItemId(rs.getInt("m.id"));
						CartList.add(beans);

					}

				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				} finally {
					// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}
				return CartList;
			}

			public static double getCartItemCount(int userId) throws SQLException {
				Connection conn = null;
				PreparedStatement st = null;
				try {
					conn = DBManager.getConnection();
					st = conn.prepareStatement("select count(*) as cnt from t_cart where user_id = ?");
					st.setInt(1, userId);

					ResultSet rs = st.executeQuery();
					double coung = 0.0;
					while (rs.next()) {
						coung = Double.parseDouble(rs.getString("cnt"));
					}
					return coung;
				} catch (Exception e) {
					System.out.println(e.getMessage());
					throw new SQLException(e);
				} finally {
					if (conn != null) {
						conn.close();
					}
				}
			}



}
