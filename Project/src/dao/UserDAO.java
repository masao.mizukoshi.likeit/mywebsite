package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import DBManager.DBManager;
import beans.UserDataBeans;

public class UserDAO {

	public List<UserDataBeans> findAll() {
		Connection conn = null;
		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id !=1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加

			while (rs.next()) {
				UserDataBeans udb = new UserDataBeans();
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setPassword(rs.getString("login_password"));
				udb.setBirthdate(rs.getDate("birth_date"));

				userList.add(udb);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public UserDataBeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and login_password = ?";
			//ハッシュを生成したい元の文字列
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			ResultSet rs = pStmt.executeQuery();

			//インスタンス生成
			UserDataBeans udb = new UserDataBeans();
			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;

			}
			udb.setId(rs.getInt("id"));
			udb.setName(rs.getString("name"));
			udb.setLoginId(rs.getString("login_id"));
			udb.setPassword(rs.getString("login_password"));
			udb.setBirthdate(rs.getDate("birth_date"));

			System.out.println("Login!");
			return udb;

		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public UserDataBeans findUserId(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT id FROM user WHERE login_id = ?";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			//インスタンス生成
			UserDataBeans udb = new UserDataBeans();
			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;

			}
			udb.setId(rs.getInt("id"));
			return udb;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザの新規登録
		public void InsertUser(String name, String loginId, String password , String birthDate) {
			Connection conn = null;
			PreparedStatement pStmt = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				String sql = "INSERT INTO user(name,login_id,login_password,birth_date) VALUES(?,?,?,?)";
				//ハッシュを生成したい元の文字列
				String source = password;
				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
				String algorithm = "MD5";

				//ハッシュ生成処理
				byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				String result = DatatypeConverter.printHexBinary(bytes);
				pStmt = conn.prepareStatement(sql);
				// SQLの?パラメータに値を設定
				pStmt.setString(1, name);
				pStmt.setString(2, loginId);
				pStmt.setString(3, result);
				pStmt.setString(4, birthDate);

				// 登録SQL実行
				pStmt.executeUpdate();
			} catch (SQLException | NoSuchAlgorithmException e) {
				e.printStackTrace();
			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズw処理を実行
					if (pStmt != null) {
						pStmt.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

		public void UpdateUser(int userId, String name, String birthdate) {
			Connection conn = null;
			PreparedStatement pStmt = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				String sql = "UPDATE user SET name=?,birth_date=? WHERE id=?";

				pStmt = conn.prepareStatement(sql);
				// SQLの?パラメータに値を設定

				pStmt.setString(1, name);
				pStmt.setString(2, birthdate);
				pStmt.setInt(3, userId);
				// 登録SQL実行
				pStmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズ処理を実行
					if (pStmt != null) {
						pStmt.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

		//ユーザ情報更新パスワードあり
		public void UpdateUserWithPassword(int userId, String name, String birthdate, String password) {
			Connection conn = null;
			PreparedStatement pStmt = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				String sql = "UPDATE user SET name=?,birth_date=?,login_password=? WHERE id=?";
				//ハッシュを生成したい元の文字列
				String source = password;
				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
				String algorithm = "MD5";

				//ハッシュ生成処理
				byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				String result = DatatypeConverter.printHexBinary(bytes);
				pStmt = conn.prepareStatement(sql);
				// SQLの?パラメータに値を設定

				pStmt.setString(1, name);
				pStmt.setString(2, birthdate);
				pStmt.setString(3, result);
				pStmt.setInt(4, userId);
				// 登録SQL実行
				pStmt.executeUpdate();
			} catch (SQLException | NoSuchAlgorithmException e) {
				e.printStackTrace();
			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズ処理を実行
					if (pStmt != null) {
						pStmt.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

		public UserDataBeans findByLoginInfo(String loginId) {
			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT * FROM user WHERE login_id = ?";
				//ハッシュを生成したい元の文字列


				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				ResultSet rs = pStmt.executeQuery();

				//インスタンス生成
				UserDataBeans udb = new UserDataBeans();
				// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
				if (!rs.next()) {
					return null;

				}
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setPassword(rs.getString("login_password"));
				udb.setBirthdate(rs.getDate("birth_date"));

				System.out.println("User Update Complete!");
				return udb;

			} catch (SQLException  e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}


		public void DeleteUser(String id) {
			Connection conn = null;
			PreparedStatement pStmt = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				String sql = "DELETE FROM user WHERE id= ?";

				pStmt = conn.prepareStatement(sql);
				// SQLの?パラメータに値を設定
				pStmt.setString(1, id);


				// 登録SQL実行
				pStmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズ処理を実行
					if (pStmt != null) {
						pStmt.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

		public List<UserDataBeans> SearchUser(String loginId, String name, String birthDate1, String birthDate2) {
			Connection conn = null;

			List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				// TODO: 未実装：管理者以外を取得するようSQLを変更する
				String sql = "SELECT * FROM user WHERE id !=1";

				if(!name.equals("")) {
					sql += " AND name LIKE '%" + name + "%'";
				}

				if(!loginId.equals("")) {
					sql += " AND login_id = '" + loginId + "'";
				}

				if(!birthDate1.equals("")) {
					sql += " AND birth_date > '" + birthDate1 + "'";
				}
				if(!birthDate2.equals("")) {
					sql += " AND birth_date < '" + birthDate2 + "'";
				}

				// SELECTを実行し、結果表を取得
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				// 結果表に格納されたレコードの内容を
				// Userインスタンスに設定し、ArrayListインスタンスに追加
				while (rs.next()) {
					UserDataBeans udb = new UserDataBeans();
					udb.setId(rs.getInt("id"));
					udb.setName(rs.getString("name"));
					udb.setLoginId(rs.getString("login_id"));
					udb.setBirthdate(rs.getDate("birth_date"));

					userList.add(udb);
				}

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return userList;
		}

}
