package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DBManager.DBManager;
import beans.WishDataBeans;

public class UserWishDAO {

	//商品を欲しいものリストへ追加
	public void AddWishItem(int userId, String itemId) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO t_wish (user_id,item_id) VALUES(?,?)";

			pStmt = conn.prepareStatement(sql);
			// SQLの?パラメータに値を設定
			pStmt.setInt(1, userId);
			pStmt.setString(2, itemId);

			// 登録SQL実行
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	//カート表示
	public List<WishDataBeans> getWishItemsByUserId(int userId,int pageNum, int pageMaxItemCount) throws SQLException {
		Connection conn = null;
		PreparedStatement st = null;
		List<WishDataBeans> WishList = new ArrayList<WishDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;



			String sql="SELECT * FROM t_wish t "
					+ "INNER JOIN m_item m "
					+ "ON t.item_id = m.id "
					+ "WHERE t.user_id =? "
					+ "ORDER BY t.id "
					+ "ASC LIMIT ?,?";
					st = conn.prepareStatement(sql);

					st.setInt(1,userId);
					st.setInt(2, startiItemNum);
					st.setInt(3, pageMaxItemCount);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				WishDataBeans beans = new WishDataBeans();

				beans.setId(rs.getInt("t.id"));
				beans.setItemId(rs.getInt("m.id"));

				WishList.add(beans);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return WishList;
	}

	public static double getWishItemCount(int userId) throws SQLException {
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DBManager.getConnection();
			st = conn.prepareStatement("select count(*) as cnt from t_wish where user_id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	//カート追加された場合、欲しいものリストから削除する

	public void DeleteWishItem(String wishId) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "DELETE FROM t_wish WHERE id=?";

			pStmt = conn.prepareStatement(sql);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, wishId);


			// 登録SQL実行
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
